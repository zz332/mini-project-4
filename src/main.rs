use actix_files::Files;
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use rand::Rng;

// Endpoint for generating a random number
async fn random_number() -> impl Responder {
    let random_number = rand::thread_rng().gen_range(0..100); // Generates a number between 0 and 99
    HttpResponse::Ok().body(format!("Random number: {}", random_number))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/random", web::get().to(random_number)) // Route to the random_number function
            .service(Files::new("/", "./static/root/").index_file("index.html"))
    })
    .bind("0.0.0.0:50505")? // Binding to all interfaces on port 50505
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{test, web, App, http, dev::Service};

    #[actix_web::test]
    async fn test_random_number() -> Result<(), actix_web::error::Error> {
        let app = App::new().route("/random", web::get().to(random_number));
        let mut app = test::init_service(app).await;

        let req = test::TestRequest::get().uri("/random").to_request();
        let resp = app.call(req).await.unwrap();

        assert_eq!(resp.status(), http::StatusCode::OK);
        Ok(())
    }
}
