# Mini Project 4

Containerize a Rust Actix Web Service. This involves building a simple web app that will generate a random number from 0 to 99.

## Components

- **Web App (hosted via a container)**: The web app has two routes:

  - **Root Route**:
    ![Root Route](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG44.jpg)

  - **Number Generating Route**:
    ![Number Generating Route](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG43.jpg)

- **Docker Image**: The package that contains the web app and all its dependencies.
  ![Docker Image](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG42.jpg)

- **Docker Container**: The running instance of the Docker image.
  ![Docker Container](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG41.jpg)

## Preparation

1. Run `cargo new miniproject4`
2. Add dependencies into `Cargo.toml`
3. Build the web app and run it using `cargo run` to test if it works
4. Create a `Dockerfile`
5. Build the Docker image using `docker build -t my_rust_app .`
6. Deploy the Docker container to make sure it works: `docker run -d -p 50505:50505 my_rust_app`
